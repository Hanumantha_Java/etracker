package com.etracker.etracker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.etracker.dto.EventRequestDto;
import com.etracker.dto.EventResponseDto;
import com.etracker.entity.EventTracker;
import com.etracker.entity.PersonTracker;
import com.etracker.exception.PersonDoesNotExistException;
import com.etracker.repository.EventTrakerRepository;
import com.etracker.repository.PersonTrackerRepository;
import com.etracker.serviceimpl.CreateEventServiceImpl;

@SpringBootTest
class EtrackerApplicationTests {

	@Mock
	PersonTrackerRepository personTrackerRepository;
	@Mock
	EventTrakerRepository eventTrakerRepository;
	@InjectMocks
	CreateEventServiceImpl createEventServiceImpl;
	EventTracker event;
	PersonTracker person;
	EventResponseDto eventResponseDto;
	EventRequestDto eventRequestDto;

	@BeforeEach
	public void setup() {

		event = new EventTracker();
		event.setEventName("movie");
		event.setSpentAmmount(200.0);
		event.setTime(LocalDateTime.now());
	}

	@Test
	public void addEventNegitiveCase() {
		eventRequestDto = new EventRequestDto();
		eventRequestDto.setAmmount(100.0);
		eventRequestDto.setEventName("movie");
		eventRequestDto.setPersonId(1);
		person = null;
		Mockito.when(personTrackerRepository.findByPersonId(Mockito.anyInt())).thenReturn(person);
		assertThrows(PersonDoesNotExistException.class, () -> createEventServiceImpl.addEvent(eventRequestDto));
	}

	@Test
	public void addEventPositiveCase() throws PersonDoesNotExistException {
		eventRequestDto = new EventRequestDto();
		eventRequestDto.setAmmount(100.0);
		eventRequestDto.setEventName("movie");
		eventRequestDto.setPersonId(1);
		person = new PersonTracker();
		person.setPersonId(1);
		person.setPersonName("simon");
		person.setEmail("simon@gmail.com");
		person.setTotalSpentAmmount(0.0);
		Mockito.when(personTrackerRepository.findByPersonId(Mockito.anyInt())).thenReturn(person);
		Mockito.when(eventTrakerRepository.save(Mockito.any(EventTracker.class))).thenReturn(event);
		person.setTotalSpentAmmount(person.getTotalSpentAmmount() + eventRequestDto.getAmmount());

		Mockito.when(personTrackerRepository.save(Mockito.any(PersonTracker.class))).thenReturn(person);

		assertEquals(200, createEventServiceImpl.addEvent(eventRequestDto).getStatusCode());
	}
}
