package com.etracker.service;

import com.etracker.dto.EventRequestDto;
import com.etracker.dto.EventResponseDto;
import com.etracker.exception.PersonDoesNotExistException;

public interface CreateEventService {
	public EventResponseDto addEvent(EventRequestDto eventRequestDto) throws PersonDoesNotExistException;
}
