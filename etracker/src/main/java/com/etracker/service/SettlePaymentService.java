package com.etracker.service;

import com.etracker.dto.SettlePaymentDto;
import com.etracker.exception.PersonDoesNotExistException;

public interface SettlePaymentService {
	public SettlePaymentDto paymetSettle(int groupId) throws PersonDoesNotExistException;

}
