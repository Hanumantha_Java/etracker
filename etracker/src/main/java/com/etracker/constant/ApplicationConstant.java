package com.etracker.constant;

public class ApplicationConstant {
	
	public static final Integer successStatusCode=200;
	public static final Integer personNotExistStatusCode=600;
	public static final String personNotExistMessage="Person doesn't exist";
	
	public static final String finalPaymentDoneSuccessfully="final payment is done per each person";
	
	private ApplicationConstant()
	{
		//avoid to create object
	}

}
