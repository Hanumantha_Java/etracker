package com.etracker.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etracker.constant.ApplicationConstant;
import com.etracker.dto.SettlePaymentDto;
import com.etracker.entity.FinalPayment;
import com.etracker.entity.GroupTracker;
import com.etracker.entity.PersonTracker;
import com.etracker.exception.PersonDoesNotExistException;
import com.etracker.repository.FinalPaymentRepository;
import com.etracker.repository.GroupTrackerRepository;
import com.etracker.repository.PersonTrackerRepository;
import com.etracker.service.SettlePaymentService;

@Service
public class SettlePaymentServiceImpl implements SettlePaymentService {

	@Autowired
	PersonTrackerRepository personTrackerRepository;
	@Autowired
	FinalPaymentRepository finalPaymentRepository;
	@Autowired
	GroupTrackerRepository groupTrackerRepository;
	private static final Logger log = LoggerFactory.getLogger(SettlePaymentServiceImpl.class);
	public SettlePaymentDto paymetSettle(int groupId) throws PersonDoesNotExistException {
		SettlePaymentDto settlePaymentDto = new SettlePaymentDto();
		FinalPayment finalPayment;
		List<FinalPayment> paymentlist = new ArrayList<>();
		List<PersonTracker> persons = personTrackerRepository.findAllByGroupGroupId(groupId);
		GroupTracker group = groupTrackerRepository.findByGroupId(groupId);
		double totalammount = 0.0;
		if (!persons.isEmpty()) {

			for (PersonTracker person : persons) {
				totalammount += person.getTotalSpentAmmount();
			}
			for (PersonTracker person : persons) {
				if (person.getTotalSpentAmmount() >= (totalammount / persons.size())) {
					finalPayment = new FinalPayment();
					finalPayment.setStatus("credit");
					finalPayment.setBalance(person.getTotalSpentAmmount() - (totalammount / persons.size()));
					finalPayment.setGroupId(groupId);
					finalPayment.setPersonId(person.getPersionId());
					paymentlist.add(finalPayment);

				} else {
					finalPayment = new FinalPayment();
					finalPayment.setStatus("debit");
					finalPayment.setBalance(person.getTotalSpentAmmount() - (totalammount / persons.size()));
					finalPayment.setGroupId(groupId);
					finalPayment.setPersonId(person.getPersionId());
					paymentlist.add(finalPayment);

				}
			}
			finalPaymentRepository.saveAll(paymentlist);
			group.setStatus("Closed");
			groupTrackerRepository.save(group);
			settlePaymentDto.setMessage(ApplicationConstant.finalPaymentDoneSuccessfully);
			settlePaymentDto.setStatusCode(ApplicationConstant.successStatusCode);
			return settlePaymentDto;

		} else {
			log.error("persons doesn't exist based on group id");
			throw new PersonDoesNotExistException("persons doesn't exist based on group id");
		}
	}

}
