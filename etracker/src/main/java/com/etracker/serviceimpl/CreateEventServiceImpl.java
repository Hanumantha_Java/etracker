package com.etracker.serviceimpl;

import java.time.LocalDateTime;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etracker.constant.ApplicationConstant;
import com.etracker.dto.EventRequestDto;
import com.etracker.dto.EventResponseDto;
import com.etracker.entity.EventTracker;
import com.etracker.entity.PersonTracker;
import com.etracker.exception.PersonDoesNotExistException;
import com.etracker.repository.EventTrakerRepository;
import com.etracker.repository.PersonTrackerRepository;
import com.etracker.service.CreateEventService;

@Service
public class CreateEventServiceImpl implements CreateEventService {

	@Autowired
	EventTrakerRepository eventTrakerRepository;
	@Autowired
	PersonTrackerRepository personTrackerRepository;
	private static final Logger log = LoggerFactory.getLogger(CreateEventServiceImpl.class);

	public EventResponseDto addEvent(EventRequestDto eventRequestDto) throws PersonDoesNotExistException {
		EventTracker evnet = new EventTracker();
		EventResponseDto eventResponseDto = new EventResponseDto();
		PersonTracker person = personTrackerRepository.findByPersonId(eventRequestDto.getPersonId());

		if (Objects.nonNull(person)) {
			evnet.setEventName(eventRequestDto.getEventName());
			evnet.setSpentAmmount(eventRequestDto.getAmmount());
			evnet.setPerson(person);
			evnet.setTime(LocalDateTime.now());
			eventTrakerRepository.save(evnet);
			person.setTotalSpentAmmount(person.getTotalSpentAmmount() + eventRequestDto.getAmmount());
			personTrackerRepository.save(person);
			eventResponseDto.setMessage("event added successfully");
			eventResponseDto.setStatusCode(ApplicationConstant.successStatusCode);
			return eventResponseDto;
		} else {
			log.error("person doesn't exist");
			throw new PersonDoesNotExistException("person doesn't exist");
		}
	}

}
