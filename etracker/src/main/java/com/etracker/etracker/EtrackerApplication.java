package com.etracker.etracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EntityScan("com.etracker.entity")
@ComponentScan("com.etracker")
@EnableJpaRepositories("com.etracker.repository")
@SpringBootApplication
public class EtrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EtrackerApplication.class, args);
	}

}
