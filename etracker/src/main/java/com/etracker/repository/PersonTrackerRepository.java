package com.etracker.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etracker.entity.PersonTracker;

public interface PersonTrackerRepository extends JpaRepository<PersonTracker, Integer>{

	PersonTracker findByPersonId(Integer personId);

	List<PersonTracker> findAllByGroupGroupId(int groupId);

}
