package com.etracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etracker.entity.GroupTracker;

public interface GroupTrackerRepository extends JpaRepository<GroupTracker, Integer>{

	GroupTracker findByGroupId(int groupId);

}
