package com.etracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etracker.entity.EventTracker;

public interface EventTrakerRepository extends JpaRepository<EventTracker, Integer>{

}
