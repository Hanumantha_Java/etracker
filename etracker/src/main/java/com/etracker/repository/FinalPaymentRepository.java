package com.etracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etracker.entity.FinalPayment;

public interface FinalPaymentRepository extends JpaRepository<FinalPayment, Integer>
{

}
