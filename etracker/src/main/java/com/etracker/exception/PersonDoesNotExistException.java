package com.etracker.exception;

public class PersonDoesNotExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	public PersonDoesNotExistException(String message) {
		super();
		this.message = message;
	}

	public PersonDoesNotExistException() {
		// create object
	}

}
