package com.etracker.exception;

import java.util.List;

public class ErrorResponse {
	private List<String> details;
	private String message;
	public List<String> getDetails() {
		return details;
	}
	public void setDetails(List<String> details) {
		this.details = details;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ErrorResponse() {
	//create object
	}
	public ErrorResponse(String string, List<String> details2) {
		// TODO Auto-generated constructor stub
	}
	
}
