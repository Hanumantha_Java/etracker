package com.etracker.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.etracker.dto.EventRequestDto;
import com.etracker.dto.EventResponseDto;
import com.etracker.dto.SettlePaymentDto;
import com.etracker.exception.PersonDoesNotExistException;
import com.etracker.service.CreateEventService;
import com.etracker.service.SettlePaymentService;

@RequestMapping("/etracker")
@RestController
public class EtrackerController {

	@Autowired
	CreateEventService createEventService;
	@Autowired
	SettlePaymentService settlePaymentService;
	
	private static final Logger log = LoggerFactory.getLogger(EtrackerController.class);


	@PostMapping("events")
	public EventResponseDto addEvents(@Valid @RequestBody EventRequestDto eventRequestDto)
			throws PersonDoesNotExistException {
		log.info("add events controller");
		return createEventService.addEvent(eventRequestDto);
	}

	@PostMapping("payments/{groupId}")

	public SettlePaymentDto paymetSettle(@PathVariable int groupId) throws PersonDoesNotExistException

	{
		log.info("payment controller");
		return settlePaymentService.paymetSettle(groupId);
	}
}
