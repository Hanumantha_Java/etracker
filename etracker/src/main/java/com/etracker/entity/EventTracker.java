package com.etracker.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class EventTracker implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer eventId;
	private String eventName;
	private Double spentAmmount;
	private LocalDateTime time;

	public EventTracker() {
		// create object
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Double getSpentAmmount() {
		return spentAmmount;
	}

	public void setSpentAmmount(Double spentAmmount) {
		this.spentAmmount = spentAmmount;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	@ManyToOne
	@JoinColumn(name = "fk_person")
	PersonTracker person;

	public PersonTracker getPerson() {
		return person;
	}

	public void setPerson(PersonTracker person) {
		this.person = person;
	}

	public EventTracker(PersonTracker person) {
		super();
		this.person = person;
	}

}
