package com.etracker.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class GroupTracker implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer groupId;
	private String groupName;
	private String status;
	private LocalDate createdDate;

	public GroupTracker() {
		// create object
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
	private Set<PersonTracker> person;

	public Set<PersonTracker> getPerson() {
		return person;
	}

	public void setPerson(Set<PersonTracker> person) {
		this.person = person;
	}

	public GroupTracker(Set<PersonTracker> person) {
		super();
		this.person = person;
	}

}
