package com.etracker.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class PersonTracker implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer personId;
	private String personName;
	private String email;
	private String phonenumber;
	private Double totalSpentAmmount;

	public PersonTracker() {
		// create object
	}

	public Integer getPersionId() {
		return personId;
	}

	public void setPersonId(Integer persionId) {
		this.personId = persionId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public Double getTotalSpentAmmount() {
		return totalSpentAmmount;
	}

	public void setTotalSpentAmmount(Double totalSpentAmmount) {
		this.totalSpentAmmount = totalSpentAmmount;
	}

	@ManyToOne
	@JoinColumn(name = "fk_group")
	GroupTracker group;

	public GroupTracker getGroup() {
		return group;
	}

	public void setGroup(GroupTracker group) {
		this.group = group;
	}

	public PersonTracker(GroupTracker group) {
		super();
		this.group = group;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
	private Set<EventTracker> event;

	public Set<EventTracker> getEvent() {
		return event;
	}

	public void setEvent(Set<EventTracker> event) {
		this.event = event;
	}

	public PersonTracker(Set<EventTracker> event) {
		super();
		this.event = event;
	}

}
