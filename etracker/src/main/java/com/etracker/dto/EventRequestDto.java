package com.etracker.dto;

import javax.validation.constraints.NotEmpty;

public class EventRequestDto {

	private Integer personId;
	@NotEmpty(message = "event name should not be empty")
	private String eventName;
	private Double ammount;

	public EventRequestDto() {
		// create object
	}

	public Double getAmmount() {
		return ammount;
	}

	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

}
