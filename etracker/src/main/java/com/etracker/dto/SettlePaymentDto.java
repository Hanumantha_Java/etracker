package com.etracker.dto;

public class SettlePaymentDto {
	private Integer statusCode;
	private String message;

	public SettlePaymentDto() {
		// create object
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
