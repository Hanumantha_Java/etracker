package com.etracker.dto;

public class EventResponseDto {
	
	private Integer statusCode;
	private String message;
	
	public EventResponseDto() {
	//create object
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
